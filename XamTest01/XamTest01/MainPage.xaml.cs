﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamTest01
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            ButtonGo.Clicked += ButtonGo_Clicked;
        }

        private void ButtonGo_Clicked(object sender, EventArgs e)
        {
            LabelMessage.Text = "Hi " + EntryName.Text;
        }
    }
}
